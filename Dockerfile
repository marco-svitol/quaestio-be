FROM node:bullseye-slim
ENV SERVERPORT=80

WORKDIR /nodejs
COPY ["package.json", "package-lock.json*", "./"]

RUN npm install

EXPOSE 80

COPY . .

CMD ["npm", "start"]
